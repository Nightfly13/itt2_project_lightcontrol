/* TSL2591 Digital Light Sensor, example with (simple) interrupt support  */
/* Dynamic Range: 600M:1 */
/* Maximum Lux: 88K */

/*  This example shows how the interrupt system on the TLS2591
 *  can be used to detect a meaningful change in light levels.
 *  
 *  Two thresholds can be set: 
 *  
 *  Lower Threshold - Any light sample on CHAN0 below this value
 *                    will trigger an interrupt
 *  Upper Threshold - Any light sample on CHAN0 above this value
 *                    will trigger an interrupt
 *                    
 *  If CHAN0 (full light) crosses below the low threshold specified,
 *  or above the higher threshold, an interrupt is asserted on the interrupt
 *  pin. The use of the HW pin is optional, though, since the change can
 *  also be detected in software by looking at the status byte via
 *  tsl.getStatus().
 *  
 *  An optional third parameter can be used in the .registerInterrupt
 *  function to indicate the number of samples that must stay outside
 *  the threshold window before the interrupt fires, providing some basic
 *  debouncing of light level data.
 *  
 *  For example, the following code will fire an interrupt on any and every
 *  sample outside the window threshold (meaning a sample below 100 or above
 *  1500 on CHAN0 or FULL light):
 *  
 *    tsl.registerInterrupt(100, 1500, TSL2591_PERSIST_ANY);
 *  
 *  This code would require five consecutive changes before the interrupt
 *  fires though (see tls2591Persist_t in Adafruit_TLS2591.h for possible
 *  values):
 *  
 *    tsl.registerInterrupt(100, 1500, TSL2591_PERSIST_5);
 */

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_TSL2591.h"

// Example for demonstrating the TSL2591 library - public domain!

// connect SCL to I2C Clock
// connect SDA to I2C Data
// connect Vin to 3.3-5V DC
// connect GROUND to common ground

// Interrupt thresholds and persistance
#define TLS2591_INT_THRESHOLD_LOWER (100)
#define TLS2591_INT_THRESHOLD_UPPER (1500)
//#define TLS2591_INT_PERSIST        (TSL2591_PERSIST_ANY) // Fire on any valid change
#define TLS2591_INT_PERSIST (TSL2591_PERSIST_60) // Require at least 60 samples to fire

Adafruit_TSL2591 tsl = Adafruit_TSL2591(2591); // pass in a number for the sensor identifier (for your use later)

/**************************************************************************/
/*x
    Displays some basic information on this sensor from the unified
    sensor API sensor_t type (see Adafruit_Sensor for more information)
*/
/**************************************************************************/

volatile bool sensor_threshold_triggered = false;
void sensor_interrupt()
{
  sensor_threshold_triggered = true;
}

void set_threshold(uint16_t thresh_low, uint16_t thresh_high, tsl2591Persist_t persist = TSL2591_PERSIST_ANY)
{
  Serial.print(thresh_low);
  Serial.print(" ");
  Serial.println(thresh_high);

  tsl.clearInterrupt();
  tsl.registerInterrupt(thresh_low, thresh_high, persist);
}

void displaySensorDetails(void)
{
  sensor_t sensor;
  tsl.getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.print("Sensor:       ");
  Serial.println(sensor.name);
  Serial.print("Driver Ver:   ");
  Serial.println(sensor.version);
  Serial.print("Unique ID:    ");
  Serial.println(sensor.sensor_id);
  Serial.print("Max Value:    ");
  Serial.print(sensor.max_value);
  Serial.println(" lux");
  Serial.print("Min Value:    ");
  Serial.print(sensor.min_value);
  Serial.println(" lux");
  Serial.print("Resolution:   ");
  Serial.print(sensor.resolution, 4);
  Serial.println(" lux");
  Serial.println("------------------------------------");
  Serial.println("");
  delay(500);
}

/**************************************************************************/
/*
    Configures the gain and integration time for the TSL2591
*/
/**************************************************************************/
void configureSensor(void)
{
  // You can change the gain on the fly, to adapt to brighter/dimmer light situations
  //tsl.setGain(TSL2591_GAIN_LOW);    // 1x gain (bright light)
  tsl.setGain(TSL2591_GAIN_MED); // 25x gain
  // tsl.setGain(TSL2591_GAIN_HIGH);   // 428x gain

  // Changing the integration time gives you a longer time over which to sense light
  // longer timelines are slower, but are good in very low light situtations!
  tsl.setTiming(TSL2591_INTEGRATIONTIME_100MS); // shortest integration time (bright light)
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_200MS);
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_300MS);
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_400MS);
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_500MS);
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_600MS);  // longest integration time (dim light)

  /* Display the gain and integration time for reference sake */
  Serial.println("------------------------------------");
  Serial.print("Gain:         ");
  tsl2591Gain_t gain = tsl.getGain();
  switch (gain)
  {
  case TSL2591_GAIN_LOW:
    Serial.println("1x (Low)");
    break;
  case TSL2591_GAIN_MED:
    Serial.println("25x (Medium)");
    break;
  case TSL2591_GAIN_HIGH:
    Serial.println("428x (High)");
    break;
  case TSL2591_GAIN_MAX:
    Serial.println("9876x (Max)");
    break;
  }
  Serial.print("Timing:       ");
  Serial.print((tsl.getTiming() + 1) * 100, DEC);
  Serial.println(" ms");
  Serial.println("------------------------------------");
  Serial.println("");

  /* Setup the SW interrupt to trigger between 100 and 1500 lux */
  /* Threshold values are defined at the top of this sketch */
  tsl.clearInterrupt();
  tsl.registerInterrupt(TLS2591_INT_THRESHOLD_LOWER,
                        TLS2591_INT_THRESHOLD_UPPER,
                        TLS2591_INT_PERSIST);

  /* Display the interrupt threshold window */
  Serial.print("Interrupt Threshold Window: ");
  Serial.print(TLS2591_INT_THRESHOLD_LOWER, DEC);
  Serial.print(" to ");
  Serial.println(TLS2591_INT_THRESHOLD_UPPER, DEC);
  Serial.println("");
}

/**************************************************************************/
/*
    Program entry point for the Arduino sketch
*/
/**************************************************************************/
void setup_default(void)
{
  Serial.begin(9600);
  // Enable this line for Flora, Zero and Feather boards with no FTDI chip
  // Waits for the serial port to connect before sending data out
  // while (!Serial) { delay(1); }

  Serial.println("Starting Adafruit TSL2591 interrupt Test!");

  if (tsl.begin())
  {
    Serial.println("Found a TSL2591 sensor");
  }
  else
  {
    Serial.println("No sensor found ... check your wiring?");
    while (1)
      ;
  }

  /* Display some basic information on this sensor */
  displaySensorDetails();

  /* Configure the sensor (including the interrupt threshold) */
  configureSensor();

  // Now we're ready to get readings ... move on to loop()!

  
  
}

/**************************************************************************/
/*
    Show how to read IR and Full Spectrum at once and convert to lux
*/
/**************************************************************************/
void advancedRead(void)
{
  // More advanced data read example. Read 32 bits with top 16 bits IR, bottom 16 bits full spectrum
  // That way you can do whatever math and comparisons you want!
  uint32_t lum = tsl.getFullLuminosity();
  uint16_t ir, full;
  ir = lum >> 16;
  full = lum & 0xFFFF;
  Serial.print("[ ");
  Serial.print(millis());
  Serial.print(" ms ] ");
  Serial.print("IR: ");
  Serial.print(ir);
  Serial.print("  ");
  Serial.print("Full: ");
  Serial.print(full);
  Serial.print("  ");
  Serial.print("Visible: ");
  Serial.print(full - ir);
  Serial.print("  ");
  Serial.print("Lux: ");
  Serial.println(tsl.calculateLux(full, ir));
}

void getStatus(void)
{
  uint8_t x = tsl.getStatus();
  // bit 4: ALS Interrupt occured
  // bit 5: No-persist Interrupt occurence
  if (x & 0x10)
  {
    Serial.print("[ ");
    Serial.print(millis());
    Serial.print(" ms ] ");
    Serial.println("ALS Interrupt occured");
  }
  if (x & 0x20)
  {
    Serial.print("[ ");
    Serial.print(millis());
    Serial.print(" ms ] ");
    Serial.println("No-persist Interrupt occured");
  }

  // Serial.print("[ "); Serial.print(millis()); Serial.print(" ms ] ");
  Serial.print("Status: ");
  Serial.println(x, BIN);
  tsl.clearInterrupt();
}

/**************************************************************************/
/*
    Arduino loop function, called once 'setup' is complete (your own code
    should go here)
*/
/**************************************************************************/

#define PIN_R1 7
#define PIN_R2 8
#define PIN_D1 2
#define PIN_D2 4
#define PIN_G 6
#define PIN_L 5
//#define PIN_I 3
#define PIN_SDA PIN_A4
#define PIN_SDC PIN_A5
#define SHUTTER_DIR_UP 1
#define SHUTTER_DIR_DOWN -1
#define SHUTTER_DIR_STOP 0
//#define SHUTTER_SPEED 255
uint8_t SHUTTER_SPEED = 255;

bool r1, r2;
bool motor_enabled = false;
uint16_t full = 0;
uint16_t visible = 0;
uint16_t ir = 0;
uint16_t target_lux = 1000;
uint8_t led_pwm_val = 0;
uint8_t led_adjust_degree = 1;
uint16_t threshold_high = 100;
uint16_t threshold_low = 50;
unsigned long motor_burst = 20;
unsigned long auto_adjust_interval = 5000;
unsigned long motor_adjust_cooldown = 300;
unsigned long motor_adjust_last = 1000;
void setup(void)
{
  setup_default();
  Serial.setTimeout(5);
  pinMode(LED_BUILTIN, OUTPUT);
  //pinMode(7, OUTPUT);
  //pinMode(8, OUTPUT);
  pinMode(PIN_R1, INPUT_PULLUP);
  pinMode(PIN_R2, INPUT_PULLUP);
  //pinMode(9, OUTPUT);
  pinMode(PIN_L, OUTPUT);
  pinMode(PIN_G, OUTPUT);
  pinMode(PIN_D1, OUTPUT);
  pinMode(PIN_D2, OUTPUT);
  //pinMode(PIN_I, INPUT);
  //attachInterrupt(digitalPinToInterrupt(PIN_I), sensor_interrupt, RISING);
  //set_threshold(target_lux - threshold_high, target_lux + threshold_high);
}
unsigned int pwmval = 0;
void set_motor_dir(int dir)
{
  switch(dir){
  case SHUTTER_DIR_DOWN:
    digitalWrite(PIN_D1, HIGH);
    digitalWrite(PIN_D2, LOW);
    break;
  case SHUTTER_DIR_UP:
    digitalWrite(PIN_D1, LOW);
    digitalWrite(PIN_D2, HIGH);
    break;
  case SHUTTER_DIR_STOP:
  default:
    //digitalWrite(PIN_D1, HIGH);
    //digitalWrite(PIN_D2, HIGH);
    digitalWrite(PIN_D1, LOW);
    digitalWrite(PIN_D2, LOW);
    break;
  }
}



bool read_param(const String instr, const String name, String* output)
{
  if (instr.startsWith(name))
  {
    *output = instr.substring(name.length());
    return true;
  }
  return false;
}

bool led_change(int change)
{
  bool ret = true;
  if(change > 0)
  {
    if(led_pwm_val + change >= 255)
    {
      led_pwm_val = 255;
      ret = false;
    }
    else
    {
      led_pwm_val += change;
    }
    
  }
  else
  {
    if(led_pwm_val <= (uint8_t)(-change))
    {
      led_pwm_val = 0;
      ret = false;
    }
    else
    {
      led_pwm_val += change;
    }
  }
  
    
  analogWrite(PIN_L, led_pwm_val);
  return ret;
  
}

void read_reeds()
{
  //r1 = digitalRead(PIN_R1) == HIGH;
  //r2 = digitalRead(PIN_R2) == HIGH;
  r1 = digitalRead(PIN_R1) == LOW;
  r2 = digitalRead(PIN_R2) == LOW;
  //r1 = r2 = false;
}

void shutter_stop()
{
  analogWrite(PIN_G,0);
  set_motor_dir(SHUTTER_DIR_STOP);
}

bool shutter_up()
{
  unsigned long t = millis();
  //dont adjust the motor constantly
  if(t - motor_adjust_last < motor_adjust_cooldown)
  {
    return true;  
  }

  read_reeds();
  if(r2)
  {
    shutter_stop();
    return false;
  }
  set_motor_dir(SHUTTER_DIR_UP);

  analogWrite(PIN_G, SHUTTER_SPEED);
  
  delay(motor_burst);
  shutter_stop();
  motor_adjust_last = t;
  
  return true;
}

bool shutter_down()
{
  unsigned long t = millis();
  //dont adjust the motor constantly
  if (t - motor_adjust_last < motor_adjust_cooldown)
  {
    return true;
  }

  read_reeds();
  if (r1)
  {
    shutter_stop();
    return false;
  }

  set_motor_dir(SHUTTER_DIR_DOWN);

  analogWrite(PIN_G, SHUTTER_SPEED);
  delay(motor_burst);
  shutter_stop();
  motor_adjust_last = t;

  return true;
}

uint16_t read_light()
{
  unsigned long ms = millis(); 
  static unsigned long last_read = millis();
  //if(ms-last_read >= 100)
  {
    last_read = ms;
    uint32_t lum = tsl.getFullLuminosity();
    full = (lum & 0xFFFF);
    ir = (lum >> 16);
    visible = full - ir;
    // Serial.println("v f ir");
    // Serial.print(visible);
    // Serial.print(" ");
    // Serial.print(full);
    // Serial.print(" ");
    // Serial.println(ir);
    //tsl.clearInterrupt();
  }
  return visible;
}

enum ADJUST_STATE
{
  ADJUST_STATE_MAX_DARK,
  ADJUST_STATE_MAX_LIGHT,
  ADJUST_STATE_OK
};

enum ADJUST_STATE adjust()
{
  //Serial.println("adjust+");
  
  ADJUST_STATE state = ADJUST_STATE_OK;
  read_light();
  Serial.print("light: ");
  Serial.println(visible);
  Serial.print("target: ");
  Serial.println(target_lux);
  
  bool start_dir = (target_lux - (int)visible) >= 0;

  if(abs(target_lux - (int)visible) > threshold_high)
  {
    Serial.print("outside thresh");
    Serial.print(abs(target_lux - (int)visible));
    Serial.print(" > ");
    Serial.println(threshold_high);
    Serial.println("ADJUSTING...");
    while(true)
    {
      if(motor_enabled)
      {
        Serial.println("doing motor things");
        //read_reeds();
        if (visible < target_lux) //too dark
        {
          if(!shutter_up())
          {
            if(!led_change(led_adjust_degree))
            {
              state = ADJUST_STATE_MAX_LIGHT;
              break;
            }  
          }
          else
          {
            led_change(-255);//maximise window light
          }
          
          
        }
        else  //too light
        {
          if (!led_change(-led_adjust_degree))
          {
            if(!shutter_down()) 
            {
              state = ADJUST_STATE_MAX_DARK;
              break;
            }  
          }
        }
      }
      else
      {
        if(visible < target_lux)
        {
          if(!led_change(led_adjust_degree))
          {
            state = ADJUST_STATE_MAX_LIGHT;
            break;
          }  
        }
        else
        {
          if(!led_change(-led_adjust_degree))
          {
            state = ADJUST_STATE_MAX_DARK;
            break;
          }  
        }  
      }
      if (abs(target_lux - (int)visible) < threshold_low)
        break;

      read_light();
      if(((target_lux - (int)visible) >= 0) != start_dir)
        break;
    }
  }
  else
  {
    Serial.print("within thresh");
    Serial.print(abs(target_lux - visible));
    Serial.print(" < ");
    Serial.println(threshold_high);
  }
  

  //set_threshold(full-threshold_high, full+threshold_high);

  //Serial.println("adjust-");
  return state;
}

void loop(void)
{
  //Serial.println("Starting");
  String instr = Serial.readString();
  String param;
  if(read_param(instr, "target_lux", &param)) //set target
  {
    target_lux = param.toInt();
  }

  if (read_param(instr, "motor_burst", &param)) //motor burst set
  {
    motor_burst = param.toInt();
  }
  if (read_param(instr, "+", &param)) //motor up test
  {
    unsigned long st = millis();
    while(1)
    {
      if(!shutter_up())
      {
        Serial.println("stopped by reed 2!");
        break;
      }
      if(millis()-st >= motor_burst)
        break;
    }
    shutter_stop();
    Serial.print(r1 ? "1" : "0");
    Serial.println(r2 ? "1" : "0");
  }
  if (read_param(instr, "-", &param)) //motor down test
  {
    unsigned long st = millis();
    while (1)
    {
      if (!shutter_down())
      {
        Serial.println("stopped by reed 1!");
        break;
      }
      if (millis() - st >= motor_burst)
        break;
    }
    shutter_stop();
    Serial.print(r1 ? "1" : "0");
    Serial.println(r2 ? "1" : "0");
  }
  if (read_param(instr, " ", &param)) //set target
  {
    shutter_stop();
  }
  if(read_param(instr, "dir", &param))  //testing
  {
    set_motor_dir(param.toInt());
  }
  if (read_param(instr, "ledstep", &param)) //led adjust speed
  {
    led_adjust_degree = param.toInt();
  }
  if (read_param(instr, "motorspeed", &param)) //led adjust speed
  {
    SHUTTER_SPEED = param.toInt();
  }
  if (read_param(instr, "motor_enable", &param)) //motor enabled
  {
    //set_motor_dir(param.toInt());
    if(param == "1")
      motor_enabled = true;
    if (param == "0")
      motor_enabled = false;
    Serial.print("motor");
    Serial.println(motor_enabled);
  }
  if (read_param(instr, "reeds", &param)) //testing
  {
    //set_motor_dir(param.toInt());
    read_reeds();
    Serial.print(r1 ? "1" : "0");
    Serial.println(r2 ? "1" : "0");
  }
  if (read_param(instr, "", &param)) //testing
  {
    //set_motor_dir(param.toInt());
    read_reeds();
    //Serial.print();
  }
  if (read_param(instr, "auto_interval", &param)) //testing
  {
    auto_adjust_interval = param.toInt();
  }
  if (read_param(instr, "burst", &param)) //testing
  {
    motor_burst = param.toInt();
  }
  unsigned long t = millis();
  static unsigned long lt = t;
  if ( (((t - lt) > auto_adjust_interval) && auto_adjust_interval !=0 ) || read_param(instr, "adj", &param)) //adjust command
  {
    lt += auto_adjust_interval;
    Serial.println("adjusting...");
    //getStatus();
    switch(adjust())
    {
      case ADJUST_STATE_MAX_DARK:
        Serial.println("max dark!");
        break;
      case ADJUST_STATE_MAX_LIGHT:
        Serial.println("max light!");
        break;
      case ADJUST_STATE_OK:
        Serial.print("adjusted to ");
        Serial.println(target_lux);
        break;
      default:
        Serial.println("???");
        break;
    }
    //Serial.println()
    //sensor_threshold_triggered = false;
    //Serial.print(millis());
    //Serial.println("sensor_threshold_triggered happened");

  }

  static unsigned long poll_interval = 100000;
  static unsigned long last_poll_time = millis();
  if(millis()-last_poll_time >= poll_interval)
  {
    last_poll_time+=poll_interval;
    Serial.print("light: "); 
    Serial.println(full);
    Serial.print("target: ");
    Serial.println(target_lux);
  }

  if (read_param(instr, "motor_adjust_cooldown", &param)) //testing
  {
    motor_adjust_cooldown = param.toInt();
  }
  if (read_param(instr, "lock", &param)) //testing
    {
      Serial.println("locking...");
      while (true)
      {
        ;
      }
    }
  //Serial.println((int)24.6 );
 

  /*  int blink_interval = read_param(instr, "blink_interval").toInt();
  Serial.println(blink_interval);
  
  int blink_interval = read_param(instr, "blink_interval").toInt();
  Serial.println(blink_interval);
*/
  /*  while(1)
  {
    int b = Serial.read();
    if(b == -1)
      break;

    s += (char)b;
  }
*/
  //advancedRead();
  //getStatus();
  
//  digitalWrite(LED_BUILTIN, HIGH);
//  delay(500);
//  digitalWrite(LED_BUILTIN, LOW);
//  delay(500);

  //analogWrite(5,(pwmval++)%256);
  //analogWrite(5,255);
}

/*
dir1
dir0
dir-1
dir2
target_lux1000
target_lux800
target_lux700
target_lux600
target_lux500
target_lux400
target_lux300
target_lux2000
target_lux4000
target_lux200
target_lux100
target_lux50
lock
adj
motor_enable1
motor_enable0
shutter_down
shutter_up
reeds
motorspeed
burst10000
burst2000
burst50
burst20
burst10
motor_adjust_cooldown150
*/