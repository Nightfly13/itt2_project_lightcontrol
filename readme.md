# Introduction
The project that you will find in this page is meant to be a lighting control system. The main idea for the project is for various lights and blinds to be adjusted automatically based on the current light level to match the desired one. For demonstration purposes the system was downscaled to a box, however, it can be fully used in a real room. This project was made by 3 people during our 2nd semester of university. 

## Parts used
- Arduino Uno 3: https://www.amazon.com/Arduino-A000066-ARDUINO-UNO-R3/dp/B008GRTSV6
- Adafruit TSL2591 Lux Sensor: https://www.digikey.dk/product-detail/en/adafruit-industries-llc/1980/1528-1037-ND/4990786
- Geared DC Motor: https://www.amazon.com/Geared-Motor-Toy-Car-Wheel/dp/B01MRLGAYD

All of these parts were used for our project, with links included for where you can buy them.

## Additional Features/Future implementation

An additional feature in the form of a website that can be used to control the system can be used on something like a Raspberry Pi 3, but this was not included due to time constraints. 

## Demo

A demonstration of the system working can be found using this link: https://www.youtube.com/watch?v=jQHPx_N7ffQ&feature=youtu.be

A note: The wiring was improved later on, but the system functioned the same. Also, the speed of the  curtain lowering can be adjusted to match needs.

## Contact info

If you have any questions, you can reach the members that worked on this project through these contacts:

### Anthony James Peak: anth0662@edu.ucl.dk
### Kasper Jensen: kasp7547@edu.ucl.dk
### Laurynas Medvedevas: laur176n@edu.ucl.dk